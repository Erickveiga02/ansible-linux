#!/bin/bash
# configuração do dominio

authconfig
--enablekrb5
--krb5kdc=meudominio
--krb5adminserver=meudominio
--krb5realm=MEUDOMINIO
--enablesssd
--enablesssdauth
--update

